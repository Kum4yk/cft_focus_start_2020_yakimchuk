import vk_api
import getpass
pswd = getpass.getpass('Password:', stream=None)
vk_session = vk_api.VkApi('+79134636437', pswd)
vk_session.auth()

vk = vk_session.get_api()
groups = ['abiturient_ef_nsu', 'mmfnsu2018', 'nsuabiturient', 'ef_nsu', 'mathematics_mechanics_department']

a = vk.users.get(user_ids=['dsoldatow'], fields=['last_seen'])
print(a)
groups_names = vk.groups.getById(group_ids=groups)
# b = vk.groups.getMembers(group_id = 'mmfnsu2018')

for i, group in enumerate(groups):
    id_users_in_group = vk.groups.getMembers(group_id=group)['items']
    users = vk.users.get(user_ids=id_users_in_group, fields=['last_seen'])
    count_all = 0
    count_ios = 0
    for user in users:
        if user.get('last_seen') is None:
            continue
        # print(user)
        if user['last_seen']['platform'] in [2, 3]:
            count_ios += 1

        if user['last_seen']['platform'] in [2, 3, 4]:
            count_all += 1
    print('*' * 50)
    print(group, groups_names[i]['name'])
    print('IOS: ', count_ios)
    print('ALL: ', count_all)
    print('%:   ', count_ios / count_all)
    print('*' * 15)
