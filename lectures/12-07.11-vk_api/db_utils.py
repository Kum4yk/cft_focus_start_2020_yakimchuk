import requests
import json
import psycopg2
from psycopg2.extras import RealDictCursor


def sql_execute(sql_give):
    conn = psycopg2.connect(dbname='test', user='test', password='test', host='localhost')
    cursor = conn.cursor(cursor_factory=RealDictCursor)
    answer = None

    print(sql_give)
    cursor.execute(sql_give)
    conn.commit()
    try:
        answer = cursor.fetchall()
    except:
        pass
    finally:
        conn.close()
        cursor.close()
        return answer
