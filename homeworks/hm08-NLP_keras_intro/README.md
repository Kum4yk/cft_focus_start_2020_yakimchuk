# Задания:
1. Успешно запустить имеющийся ноутбук

2. Запустить этот же ноутбук на данных tkk

3. Нормализовать данные, убрать пунктуацию

4. Реализовать свёрточную нейронную сеть для анализа твитов

5. Обернуть обученную модель в predictor.py
___
# Решение

- [Тертрадка с повторным запуском и на tkk данных](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm08-NLP_keras_intro/01.%20intro%20and%20tkk.ipynb)

- [Тетрадка с обработкой данных](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm08-NLP_keras_intro/02.%20data_preparing.ipynb) и [.py файл](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm08-NLP_keras_intro/data_handle.py) с реализованным векторизированным методом для обработки входных данных

- [Тетрадка с нейронной сетью](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm08-NLP_keras_intro/03.%20neural%20network.ipynb)

- [Предиктор](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm08-NLP_keras_intro/predicor.py)

- [Тетрадь с итоговой моделью для предиктора](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm08-NLP_keras_intro/create_predict_model.ipynb)

- [Итоговая модель](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/tree/master/homeworks/hm08-NLP_keras_intro/dl_model)