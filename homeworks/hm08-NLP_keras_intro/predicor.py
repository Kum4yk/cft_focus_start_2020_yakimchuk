from tensorflow import keras


class Predictor:
    def __init__(self, path_to_model, loss=None):
        self.model = keras.models.load_model(path_to_model)

    def predict(self, X):
        return self.model.predict(X)
