from sklearn.model_selection import cross_val_score
import pandas as pd
from itertools import combinations


class SequentialForwardSelection:
    # Т.к. в задании про оценку качества сказано не было
    # то проще использовать готовый метод,
    # в моем случае это будет cv_score
    def __init__(self, estimator, k_features: int, scoring=None, cv=5, n_jobs=-1):
        self.estimator = estimator
        self.k_features = k_features
        self.scoring = scoring
        self.cv = cv
        self.n_jobs = n_jobs
        self.is_fitted = None
        self.features = None
        self.selected_features = None

    def fit(self, data: pd.DataFrame, target, eps=0.001, debugging=False):
        assert 1 <= self.k_features <= data.shape[1], \
            "number of features must be in range [1, n_features]"  # ValueError is preferred
        self.is_fitted = True
        self.features = data.columns

        if self.k_features == data.shape[1]:
            self.selected_features = list(range(0, data.shape[1]))
            return

        first_n_score = self.__find_n_best_features(data, target, debugging=debugging)
        if len(self.selected_features) == self.k_features:
            return
        self.__greedy_adding_features(data, target, first_n_score, eps, debugging=debugging)

    def __greedy_adding_features(self, data, target, started_score, eps, debugging=False):
        best_score = started_score

        while len(self.selected_features) < self.k_features:
            previous_score = best_score
            features = list(
                index for index in range(0, data.shape[1]) if index not in set(self.selected_features)
            )
            best_feature = None

            if debugging:
                print(self.selected_features, features, best_feature, best_score)

            for i in features:
                current_features = self.selected_features[:]
                current_features.append(i)
                train = data.iloc[:, current_features]
                current_score = self.get_eval_score(train, target)

                if debugging:
                    print(best_score, best_feature, current_score, current_features, i)

                best_score, best_feature = (best_score, best_feature) \
                    if best_score > current_score else \
                    (current_score, i)

            if abs(previous_score - best_score) < eps:
                """
                Качество модели перестаёт значимо расти.
                """
                break

            if best_feature is not None:
                self.selected_features.append(best_feature)
            else:
                break

            if debugging:
                print()

    def __find_n_best_features(self, data: pd.DataFrame, target, debugging=False):
        best_n_features = list()
        best_score = -float('inf')
        n = max(1, self.k_features // 2)

        for labels in combinations(range(data.shape[1]), n):
            current_n_features = list(labels)
            train = data.iloc[:, current_n_features]

            current_score = self.get_eval_score(train, target)

            if debugging:
                print(best_n_features, best_score, " | ", current_n_features, current_score)

            best_n_features, best_score = (best_n_features, best_score) \
                if best_score > current_score else \
                (current_n_features, current_score)
        if debugging:
            print("Done n feature selection\n")

        self.selected_features = best_n_features
        return best_score

    def get_eval_score(self, train, target):
        # or min for more tough strategy, or use p-value methods
        return cross_val_score(self.estimator, train, target,
                               scoring=self.scoring, n_jobs=self.n_jobs
                               ).mean()

    def transform(self, new_data: pd.DataFrame):
        assert self.is_fitted is not None, "Do fit before transform"
        return new_data.iloc[:, self.selected_features]

    def get_selected_features(self):
        assert self.is_fitted is not None, "Do fit before"
        return self.features[self.selected_features]
