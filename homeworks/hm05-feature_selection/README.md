# hm feature selection

Реализовать класс с алгоритмом Sequential Forward Selection, который на вход будет принимать модель и количество фичей (estimator, k_features) и иметь методы fit(X,y) и transform(X).

- Фиксируем небольшое число признаков N.

- Перебираем все комбинации по N признаков, выбираем лучшую комбинацию.

- Перебираем комбинации из N+1 признаков так, что предыдущая лучшая комбинация признаков зафиксирована, а перебирается только новый признак.

- Таким образом перебираем, пока не упремся в максимально допустимое число признаков (k_features) или пока качество модели не перестанет значимо расти.
___
# Solution

1. Класс реализован в [.py файле](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm05-feature_selection/feature_selection.py)
2. Пример использования класса продемонстрирован в [тетрадке](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm05-feature_selection/Yakimchuk_hm_feature-selection.ipynb)