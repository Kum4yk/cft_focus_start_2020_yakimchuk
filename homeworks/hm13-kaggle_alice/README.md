# Task
1. Нужно скачать данные;
2. Поанализировать данные и заполнить форму  - Форма для ваших ответов;
3. Построить модель, сделать сабмит и прислать скрин с результатом мне.

# Solution 
- [01.form_solution.ipynb](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm13-kaggle_alice/01.form_solution.ipynb) - решения для гугл формы  
- [02.Kaggle_alice.ipynb](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm13-kaggle_alice/02.Kaggle_alice.ipynb) - модель для kaggle alice
- ![place](data/2_try.PNG)

