import numpy as np
import matplotlib.pyplot as plt


class SimpleLinearRegression:
    size = 100  # test value
    X: np.ndarray = 5 * np.random.rand(size, 1)
    X_extend: np.ndarray = np.hstack((np.ones((size, 1)), X))
    X_T: np.ndarray = X_extend.T

    def __init__(self, decimal_places=3):
        self._y_true = None
        self.y = None
        self.__shift = None
        self.__tan = None
        self.decimal_places = decimal_places
        self._create_equation()

    def _create_equation(self):
        """
        function to recreate linear regression parameters
        """
        self.__tan = np.round(np.random.uniform(-2, 2), self.decimal_places)
        self.__shift = np.round(np.random.uniform(-2, 2), self.decimal_places)
        self._y_true = self.__shift + self.__tan * self.X
        self.y = self._y_true + np.random.rand(SimpleLinearRegression.size, 1) - 0.5

    def theoretical_solution(self):
        result = np.round(
            np.linalg.inv(
                self.X_T.dot(self.X_extend)
                ).dot(self.X_T).dot(self.y),
            self.decimal_places
        )
        shift, tan = result[0][0], result[1][0]
        return shift, tan

    def plot_lines(self, *ys, plt_func=plt.plot, dpi=100, **labels):
        # по хорошему отрисовка графиков не является зоной ответственности данного класса
        plt.figure(dpi=dpi)
        plt.plot(self.X, self._y_true, label="ideal line", c='r')
        if ys:
            for y, label_key in zip(ys, labels):
                plt_func(self.X, y, label=labels[label_key])
        plt.legend()

    @staticmethod
    def calc_gradient(x_extend: np.ndarray, x_transpose: np.ndarray,
                      y: np.ndarray, theta, batch_size):
        # В нашем случае транспонированная матрица заранее расчитана,
        # поэтому в подписи метода она присутсвует, а не расчитывается в теле метода
        return 2 / batch_size * x_transpose.dot(x_extend.dot(theta) - y)

    def batch_gd(self, learning_rate=0.1, n_iter=100):
        theta = np.random.rand(2, 1)
        for _ in range(n_iter):
            grad = self.calc_gradient(self.X_extend, self.X_T,
                                      self.y, theta, batch_size=self.X.shape[0])
            theta = theta - learning_rate * grad
        shift, tan = self.__get_round_shift_tan(theta)
        return shift, tan

    def mini_batch_gd(self, learning_rate=0.1, n_iter=100,
                      mini_batch_fraction=0.1):
        theta = np.random.rand(2, 1)
        batch_size = round(self.X.shape[0] * mini_batch_fraction)
        indexes = np.random.choice(
            np.arange(self.X.shape[0]),
            size=batch_size * n_iter
        )

        for i in range(n_iter):
            ixs = indexes[i*batch_size:(i+1)*batch_size]
            X_e = self.X_extend[ixs]
            X_T = self.X_T[:, ixs]
            y = self.y[ixs]
            grad = self.calc_gradient(X_e, X_T,
                                      y, theta, batch_size)
            theta = theta - learning_rate * grad

        shift, tan = self.__get_round_shift_tan(theta)
        return shift, tan

    def stochastic_gd(self, learning_rate=0.1, n_iter=100):
        theta = np.random.rand(2, 1)
        ixs = np.random.choice(
            np.arange(self.X.shape[0]),
            n_iter
        )[:, np.newaxis]

        for i in ixs:
            X_e = self.X_extend[i]
            X_T = self.X_T[:, i]
            y = self.y[i]
            grad = self.calc_gradient(X_e, X_T,
                                      y, theta, batch_size=1)
            theta = theta - learning_rate * grad

        shift, tan = self.__get_round_shift_tan(theta)
        return shift, tan

    def __get_round_shift_tan(self, theta: np.ndarray):
        theta = np.round(theta.copy(), self.decimal_places)
        shift, tan = theta[0][0], theta[1][0]
        return shift, tan


if __name__ == "__main__":
    print("Hello")
    some = SimpleLinearRegression()
    print(some.__dict__.keys())
