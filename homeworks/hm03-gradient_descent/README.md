# Домашнее задание 
___
Сравнить по времени разные реализации градиентного спуска и нормального уравнения в линейной регрессии:

1. Сгенерируйте облако рассеяния. Возьмите любое уравнение прямой и добавьте гаусовского шума.

2. Попробуйте найти решение через нормальное уравнение. Замерьте время исполнения кода при помощи %%time или %%timeit.

3. Реализуйте метод для вычисления градиента.

4. Реализуйте на основе этого метода пакетный, мини-пакетный и стохастический градиентный спуск.

5. Запишите результаты времени выполнения кода в одну таблицу и укажите количество эпох обучения

___

# Решение

1. Класс с расчетными методами реализован в [.py файле](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm03-gradient_descent/simple_linear_regression.py)
 
2. Отрисованная таблица в конце [тетрадки](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm03-gradient_descent/Yakimchuk.homework_gradient-descent.ipynb) и в [.csv файле.](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm03-gradient_descent/Yakimchuk_gradient_descent_table.csv)