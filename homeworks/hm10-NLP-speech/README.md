# Task:

1. Успешно запустить имеющийся ноутбук

2. Скачать [предобученную модель для русской речи](http://alphacephei.com/kaldi/models/vosk-model-ru-0.10.zip), переименовать извлечённую папку с данными в model и расположить её относительно ноутбука в ../

3. До вечера четверга записать 6 примеров аудио (прочитать по 3 примера из каждого датасета, по 1 примеру на каждый класс - положительный, отрицательный и нейтральный) Аудио должны быть формата [моно PCM](https://stackoverflow.com/questions/13358287/how-to-convert-any-mp3-file-to-wav-16khz-mono-16bit)

4. Использовать предобученную модель для распознавания сентимента записанных "голосовых" твитов, для этого модифицировать функцию predict_sentiment

5. [Адаптировать]( https://alphacephei.com/vosk/adaptation) языковую модель с помощью тех текстов, которые есть в данных bank и ttk. Это делается в командной строке с помощью бинарников из инструментов, которые поставляются вместе с Kaldi - (http-alphacephei.com-kaldi-models-vosk-model-ru-0.10.zip.url)

___
# Solution

1. [01. Speech_recognition_and_sentiment]() - Тетрадка с работой модели

2. [02. Create audio files]() -Выборка и создание аудиофайлов 