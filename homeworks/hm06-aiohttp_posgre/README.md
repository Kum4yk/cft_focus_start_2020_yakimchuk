# task
[lecture example](https://gitlab.com/AlexeyAlymov95/my-service)
1. Настроить пул соединений с локальной БД Postgres согласно примеру: [Connection Pools](https://magicstack.github.io/asyncpg/current/usage.html#connection-pools)

2. При старте сервис должен делать запрос на создание таблицы users (если она не существует) со столбцами id, login, password. Создать 5 хендлеров, предоставляющих управление таблицей users согласно REST API:

    - Просмотр списка всех пользователей (id, login). В параметрах запроса ожидать [offset и limit](https://postgrespro.ru/docs/postgrespro/9.5/queries-limit), задающие диапазон строк.
  
    - Запрос пароля пользователя по его id, который ожидается в match_info.
    - Добавление нового пользователя. Данные ожидать в формате json.
    - Изменение пароля пользователя по его id. Ожидать match_info и json.
    - Удаление пользователя.

Не забудьте обеспечить валидацию запросов и ответов, обработку ошибок.
___
# Solution

[Link](https://gitlab.com/Kum4yk/aiohttp_hm)