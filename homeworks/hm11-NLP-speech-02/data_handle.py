from pymystem3 import Mystem
import pandas as pd
import xmltodict
import re
import numpy as np


class HandleData:
    companies = {}
    lemmas_dict = dict()
    mystem = Mystem()
    columns = 31

    @staticmethod
    def table_from_xml(xml_path, mode=None):
        assert mode in {"tkk", "bank"}

        def get_sample_text(sample):
            assert sample['column'][3]['@name'] == 'text'
            return sample['column'][3]['#text']

        def get_sample_answers_bank(sample):
            answers = {}
            for i in range(4, 12):
                HandleData.companies[sample['column'][i]['@name']] = i
                answers[sample['column'][i]['@name']] = None if sample['column'][i]['#text'] == 'NULL' \
                    else int(sample['column'][i]['#text'])
            return answers

        def get_sample_answers_tkk(sample):
            answers = {}
            for i in range(4, 11):
                HandleData.companies[sample['column'][i]['@name']] = i
                answers[sample['column'][i]['@name']] = None if sample['column'][i]['#text'] == 'NULL' \
                    else int(sample['column'][i]['#text'])
            return answers

        def get_sample_id(sample):
            assert sample['column'][0]['@name'] == 'id'
            return int(sample['column'][0]['#text'])

        def get_data(filename, answer_func=None):
            df = pd.DataFrame()
            with open(filename, "r", encoding='utf-8') as f:
                d = xmltodict.parse(f.read(), process_namespaces=True)
                clean_samples = []
                for sample in d['pma_xml_export']['database']['table']:
                    sample_id = get_sample_id(sample)
                    text = get_sample_text(sample)
                    answers = answer_func(sample)
                    for company, answer in answers.items():
                        if answer is not None:
                            clean_samples.append((sample_id, text, company, answer))
                df['text'] = [sample[1] for sample in clean_samples]
                df['answer'] = [sample[3] for sample in clean_samples]
                df['company'] = [sample[2] for sample in clean_samples]
                df['sample_id'] = [sample[0] for sample in clean_samples]
            return df

        url_replacement = lambda x: re.sub(r'(?:http[^\s]+)($|\s)', r'url\1', x)
        user_replacement = lambda x: re.sub(r'(?:@[^\s]+)($|\s)', r'user\1', x)

        func = get_sample_answers_tkk if mode == "tkk" else get_sample_answers_bank
        data = get_data(xml_path, func)

        data['text'] = data['text'].apply(url_replacement)
        data['text'] = data['text'].apply(user_replacement)

        return data.text, data.answer

    @staticmethod
    def transform_table(table: pd.Series, fasttext_model, debug_mode=False):
        def tag_by_dict(word, mystem: Mystem, words_dict, debugging=False):
            if word is None:
                return ""
            if word in words_dict:
                return words_dict[word]
            if word.isdigit():
                words_dict[word] = word
                return word

            processed = mystem.analyze(word)[0]
            if "analysis" not in processed:
                if debugging:
                    print(word, "- strange word, has not analysis")
                words_dict[word] = word
                return word

            lemma = processed["analysis"]
            if lemma:
                answer = lemma[0]["lex"].lower().strip()
                words_dict[word] = answer
                return answer

            if debugging:
                print(word, "- strange word, empty analysis")
            words_dict[word] = word
            return word

        X: pd.DataFrame = table.str.lower().replace(r'[^\w\s]', value="", regex=True) \
            .str.split(expand=True)

        X = X.applymap(lambda x: tag_by_dict(x, HandleData.mystem,
                                             HandleData.lemmas_dict, debugging=debug_mode
                                             ))

        if len(X.columns) < HandleData.columns:
            for i in set(range(31)) - set(X.columns):
                X[i] = ""
        elif len(X.columns) > HandleData.columns:
            X = X.iloc[:, :HandleData.columns]

        X = X.applymap(lambda word: np.array(fasttext_model[word]))

        X = np.array(
            [[np.array(line) for line in row] for row in X.values]
        )[:, :, :, np.newaxis]

        return X

    @staticmethod
    def get_tensor_y(y):
        mapper = {-1: np.array((1, 0, 0)),
                  0: np.array((0, 1, 0)),
                  1: np.array((0, 0, 1))
                  }
        tensor_y = np.array([mapper[i] for i in y])  # DataFrame.values not working correct

        return tensor_y
