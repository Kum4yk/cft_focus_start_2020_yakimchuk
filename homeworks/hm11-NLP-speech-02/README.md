# NLP speech recognition

- [01.model_adaptation](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm11-NLP-speech-02/01.model_adaptation.ipynb) - тетрадка с описанием того как адаптировать языковую модель

- [02.speech_recognition](https://gitlab.com/Kum4yk/cft_focus_start_2020_yakimchuk/-/blob/master/homeworks/hm11-NLP-speech-02/02.speech_recognition.ipynb) - тетрадка с примерами работы адаптированной и нет моделей
