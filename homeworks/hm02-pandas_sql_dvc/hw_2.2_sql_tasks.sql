/*
ЗАДАНИЕ 1: Посчитайте количество зданий, где установлено 2 и более типа счетчиков.
*/

SELECT
    building_id,
    COUNT(DISTINCT meter)
FROM measurement_results
GROUP BY building_id
HAVING COUNT(DISTINCT meter) >= 2
------------------------------------------------------------------------------------------------------------
/*
ЗАДАНИЕ 2 (JOIN): Представим, что мы решили построить модель на этих данных и хотим собрать все признаки в кучу
за последние день 2016 года. (если менее 5гб свободной оперативки, можете добавить ещё фильтры к основной таблице)
Соединить между собой все три таблицы (measurement_results, building_metadata, weather_train), 
с фильтром по дате от 30 декабря 2016 года. 
Учитывайте, что measurement_results является основной таблицей, где находится целевая переменная. 
*/

SELECT *
FROM weather_train as w
RIGHT JOIN (
        SELECT *
        FROM building_metadata as b
        RIGHT JOIN (
                SELECT *
                FROM measurement_results
                WHERE timestamp_measurement BETWEEN '2016-11-30 00:00:00' AND '2016-11-30 23:59:59'
                ) m
        ON b.building_id = m.building_id
        ) res
ON w.site_id = res.site_id AND w.timestamp_measurement = res.timestamp_measurement
WHERE w.timestamp_measurement BETWEEN '2016-11-30 00:00:00' AND '2016-11-30 23:59:59'
------------------------------------------------------------------------------------------------------------
/*
ЗАДАНИЕ 3 (WITH): Давайте по генерируем гипотезы на основе наших данных! (не более 2ух - не нужно придумывать, те которые сложно посчитать).
Попробуем используя конструкцию WITH в одном запросе получить:
	2.1 Основную часть данных, которая состоит из:
		- идентификатор здания
		- тип счетчика
		- целевая переменная (показания счетчика)
	2.2 Запрос насчитывающий признак №1
	2.3 Запрос насчитывающий признак №2
Объединить все в один набор данных.
*/

WITH
first as (
    SELECT
        building_id as id_first,
    COUNT(DISTINCT meter) as first_feature
    FROM measurement_results
    GROUP BY id_first
),
second as (
    SELECT
        building_id as id_second,
    MIN(meter_reading) as second_feature
    FROM measurement_results
    GROUP BY id_second
)
SELECT building_id, meter, meter_reading, first_feature, second_feature
FROM measurement_results as m
LEFT JOIN first as f ON m.building_id = f.id_first
LEFT JOIN second as s ON m.building_id = s.id_second
ORDER BY building_id ASC
------------------------------------------------------------------------------------------------------------
/*
ЗАДАНИЕ 4: Посчитайте накопительную сумму(используя оконную функцию) для каждого здания и типа счетчика.
*/

SELECT *,
    SUM(meter_reading)
        OVER(
            PARTITION BY building_id, meter
            ORDER BY timestamp_measurement
            ) as acc_sum
    FROM measurement_results

------------------------------------------------------------------------------------------------------------
/*
ЗАДАНИЕ 5 (WITH): Напишите запрос(используя оконные функции),
который уберет все дублирующие записи по полям building_id, meter 
и вернет самое новое значение.
*/

SELECT building_id, meter, timestamp_measurement, meter_reading
FROM (
    SELECT *,
        ROW_NUMBER()
        OVER(
            PARTITION BY building_id, meter
            ORDER BY timestamp_measurement DESC
            ) AS rank
        FROM measurement_results
        ORDER BY rank ASC
) AS A
WHERE rank=1
