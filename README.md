# Лекции и домашние задания по проекту профессионального развития Focus start секции Машинного обучения Центра Финансовых Технологий.
___
### [Lectures](lectures)

___
### [Homeworks](homeworks)

___
### [Final project](homeworks/hm16-final-project)
- [Simple research](https://gitlab.com/Kum4yk/cft_final_research)
- [Final docker service](https://gitlab.com/Kum4yk/final_docker)

___
### [Certificate](Cetrificate.Yakimchuk.pdf)
<img src="certificate.png" alt="certificate" height="800">